package net._95point2.core;

import java.math.BigInteger;
import java.util.Random;

import org.jasypt.util.text.BasicTextEncryptor;


public class Crypto 
{
	private final String pw;
	
	public Crypto(String passphrase) {
		this.pw = passphrase;
	}
	
	public String getEncryptedToken(String token){
		return getReadyEncryptor().encrypt(token);
	}
	
	public String getRandomString(int length)
	{
		return new BigInteger(length*5, new Random()).toString(32);
	}
	
	
	public String getDecryptedToken(String cipher){
		return getReadyEncryptor().decrypt(cipher);
	}
	
	private BasicTextEncryptor getReadyEncryptor(){
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword(pw);
		
		return textEncryptor;
	}
}
