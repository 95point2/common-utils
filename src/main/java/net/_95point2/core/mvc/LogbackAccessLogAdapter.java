package net._95point2.core.mvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.access.spi.IAccessEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import ch.qos.logback.core.status.Status;

public class LogbackAccessLogAdapter extends AccessLogToMainLogAppender
{
	Logger logger = LoggerFactory.getLogger("RequestLog");
	
	@Override
	public void log(String msg) {
		logger.info(msg);
	}

	@Override
	public void status(Status status) {
		logger.error("{}", status, new Throwable());
	}
	
	public LogbackAccessLogAdapter() {
		addFilter(new Filter<IAccessEvent>() {
			
			@Override
			public FilterReply decide(IAccessEvent event) {
				if(event.getRequestURI().startsWith("/xgen")){
					return FilterReply.DENY;
				}
				else {
					return FilterReply.NEUTRAL;
				}
			}
		});
	}
}
