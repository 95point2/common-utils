import static org.junit.Assert.*;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import net._95point2.core.obj.DateRange;


public class TestDateRangeType {

	@Test
	public void testDateRangeParser() throws Exception {
		
		DateRange dr1 = DateRange.parse("[2015-12-01,2016-12-01)");
		
		assertThat(dr1.getStartInclusive(), CoreMatchers.is(true));
		assertThat(dr1.getEndInclusive(), CoreMatchers.is(false));
		
		DateRange dr2 = DateRange.parse("(2015-12-01,2016-12-01)");
		DateRange dr3 = DateRange.parse("[2015-12-01,2016-12-01]");
		DateRange dr4 = DateRange.parse("[2015-12-01,2016-12-01]");
		
		
	}

}
