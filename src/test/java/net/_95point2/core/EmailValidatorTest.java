package net._95point2.core;


import org.junit.Assert;
import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testIsValid() {
		Assert.assertTrue(EmailValidator.isValid("rgshepherd@gmail.com"));
		Assert.assertTrue(EmailValidator.isValid("rgshepherd+test@gmail.com"));
		Assert.assertTrue(EmailValidator.isValid("chris@word.another-word.co.uk"));
		Assert.assertFalse(EmailValidator.isValid("@abc.com"));
		Assert.assertFalse(EmailValidator.isValid("rs@abc."));
		Assert.assertTrue(EmailValidator.isValid("IH53IJ64KC29@blackhole.952cloud.com"));
	}

}
