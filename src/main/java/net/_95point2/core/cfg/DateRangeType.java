package net._95point2.core.cfg;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import net._95point2.core.obj.DateRange;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

@Slf4j
public class DateRangeType implements UserType
{
	private static final int[] TYPES = new int[] { Types.VARCHAR };
	
	
	@Override
	public int[] sqlTypes() {
		return TYPES;
	}

	@Override
	public Class returnedClass() {
		return DateRange.class;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException 
	{
		if( x == y ){
			return true;
		}
		
		if( !(x instanceof DateRange) || !(y instanceof DateRange) ){
			return false;
		}
		
		DateRange drX = (DateRange)x;
		DateRange drY = (DateRange)y;
		
		if( ! drX.getStart().equals(drY.getStart())){ return false; }
		if( ! drX.getEnd().equals(drY.getEnd())){ return false; }
		if( ! drX.getStartInclusive().equals(drY.getStartInclusive())){ return false; }
		if( ! drX.getEndInclusive().equals(drY.getEndInclusive())){ return false; }
		
		return true;
	}
	
	@Override
	public int hashCode(Object x) throws HibernateException {
		return Arrays.hashCode( ((List)x).toArray() );
	}

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
		if(rs.wasNull()){
			return null;
		}
		
		String range = rs.getString(names[0]);
		
		if(range != null){
				
				try {
					return DateRange.parse(range);
				}
				catch(Exception e){
					throw new HibernateException(e);
				}
		}
		else {
			return null;
		}
	}

	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
		throw new UnsupportedOperationException("Bang! to you my friend");
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		if(value == null){
		    return null;
		}
	    if(value instanceof DateRange)
	    {
			DateRange d = new DateRange();
			d.setStart((Date) ((DateRange) value).getStart().clone());
			d.setEnd((Date) ((DateRange) value).getEnd().clone());
			d.setStartInclusive(((DateRange) value).getStartInclusive());
			d.setEndInclusive(((DateRange) value).getEndInclusive());
			return d;
		}
	    else
	    {
	    	throw new IllegalArgumentException("Not a DateRange<>: " + value.getClass());
	    }
		
	}

	@Override
	public boolean isMutable() {
		return false;
	}

	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return new Gson().toJson(value);
	}

	@Override
	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return new Gson().fromJson((String) cached, new TypeToken<ArrayList>() {}.getType());
	}

	@Override
	public Object replace(Object original, Object target, Object owner)
			throws HibernateException {
		return deepCopy(original);
	}
}
