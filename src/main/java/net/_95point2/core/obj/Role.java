package net._95point2.core.obj;

public interface Role {
	
	String getName();
	String getTeam();
	String getData();
	
}
