package net._95point2.core.cfg;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.annotation.JsonFormat;

@Retention(RetentionPolicy.RUNTIME) // IMPORTANT
@JacksonAnnotationsInside
@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ssZ", timezone="Europe/London")
public @interface ISO8601 {

}
