package net._95point2.core.obj;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Data;

@Data
public class DateRange 
{
	private Date start;
	private Date end;
	
	private Boolean startInclusive = true;
	private Boolean endInclusive = true;
	
	
	public static DateRange parse(String dateRangeStr) throws Exception
	{
		Matcher m = pattern.matcher(dateRangeStr);
		
		if(m.matches())
		{
			String startToken = m.group(1);
			String startDateString = m.group(2);
			String endDateString = m.group(3);
			String endToken = m.group(4);
			
			DateRange d = new DateRange();
			d.setStart(FORMAT.parse(startDateString));
			d.setEnd(FORMAT.parse(endDateString));
			d.setStartInclusive(startToken.equals("["));
			d.setEndInclusive(endToken.equals("]"));
			return d;
		}
		else
		{
			throw new Exception("Could not parse date range format: " + dateRangeStr);
		}
	}
	
	public boolean overlaps(DateRange then)
	{
		return !(
		 (this.getStart().getTime() >= then.getEnd().getTime())
		||
		(this.getEnd().getTime() <= then.getStart().getTime())
		);
	}
	
	public DateRange(){}
	
	public DateRange(Date start, Date end) {
		this.start = start;
		this.end = end;
	}

	private static Pattern pattern = Pattern.compile("^([\\(\\[])([0-9]{4}-[0-9]{2}-[0-9]{2}),([0-9]{4}-[0-9]{2}-[0-9]{2})([\\)\\]])");
	public final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
}
