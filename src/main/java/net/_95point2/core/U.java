package net._95point2.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class U 
{
	public static final String JSON = MediaType.APPLICATION_JSON_VALUE;
	public static final String TEXT = MediaType.TEXT_PLAIN_VALUE;
	
	public static <T,V> MapBuilder<T,V> map(T key, V val){
		return new MapBuilder<T,V>(key, val);
	}
	
	public static <T,V extends Object> MapBuilder<T,Object> omap(T key, Object val){
		return new MapBuilder<T,Object>(key, val);
	}
	
	public static class MapBuilder<T,V>{
		Map<T, V> map = new HashMap<T,V>();
		
		public MapBuilder(){
			
		}
		
		public MapBuilder(T key, V value){
			map.put(key, value);
		}
		
		public MapBuilder<T,V> and(T key, V val ){
			map.put(key, val);
			return this;
		}
		
		public Map<T,V> done(){
			return map;
		}
	}
	
	public static <T> ListBuilder<T> list(T t){
		ListBuilder<T> lb = new ListBuilder<T>();
		return lb.and(t);
	}
	
	public static <T> ListBuilder<T> list(T... t){
		ListBuilder<T> lb = new ListBuilder<T>();
		for(T it : t){
			lb.and(it);
		}
		return lb;
	}
	
	public static class ListBuilder<T> {
		private List<T> list = new ArrayList<T>();
		
		public ListBuilder<T> and(T t){
			list.add(t);
			return this;
		}
		
		public List<T> done(){
			return list;
		}
	}
	
	public static class Types {
		public static final String JSON = MediaType.APPLICATION_JSON_VALUE;
	}
	
	public static <T extends Object> String concat(Iterable<T> objects, String seperator)
	{
		StringBuilder sb = new StringBuilder();
		for(Iterator<T> it = objects.iterator(); it.hasNext(); ){
			sb.append(it.next());
			if(it.hasNext()){
				sb.append(seperator);
			}
		}
		return sb.toString();
	}

    public static void dump(Object o) 
    {
        try {
            System.out.println(new ObjectMapper().writeValueAsString(o));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        
    }
    
    public static String dumpAsStr(Object o) 
    {
        try {
            return new ObjectMapper().writeValueAsString(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "[error]";
        }
        
    }
    
    public static Iterable<String> oidAdapter(final Iterable<ObjectId> oids){
        return new Iterable<String>() {
            
            @Override
            public Iterator<String> iterator() {
                
                return new Iterator<String>() 
                {
                    Iterator<ObjectId> oidIt = oids.iterator();

                    @Override
                    public boolean hasNext() {
                        return oidIt.hasNext();
                    }

                    @Override
                    public String next() {
                        return oidIt.next().toString();
                    }
                };
            }
        };
        
    }

    public static ObjectId oid(String id) {
        return new ObjectId(id);
    }

    /**
     * make a list from an iterator
     * @param iterable
     * @return
     */
	public static <T> List<T> list(Iterable<T> iterable) 
	{
		if(iterable instanceof List){
			return (List<T>) iterable;
		}
		else return Lists.newArrayList(iterable);
	}
	
	public static <T> T coalesce(T a, T b){
		if(a == null){
			return b;
		}
		return a;
	}

	/**
	 * Null safe equals
	 * @param a
	 * @param b
	 * @return
	 */
	public static boolean nseq(Object a, Object b)
	{
	    if(a == null && b== null){
		return true;
	    }
	    if(a == null){ // inferring b != null 
		return false;
	    }
	    if(a == b){
		return true;
	    }
	    if(a.equals(b)){
		return true;
	    }
	    return false;
	}
}
