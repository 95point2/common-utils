package net._95point2.core.mvc;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.BeansException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfToken;

import com.google.common.base.Preconditions;
import com.mitchellbosecke.pebble.extension.AbstractExtension;
import com.mitchellbosecke.pebble.extension.Function;
import com.mitchellbosecke.pebble.extension.escaper.SafeString;
import com.mitchellbosecke.pebble.spring4.util.ViewUtils;

public class SpringSecPebbleExtension extends AbstractExtension
{
		@Override
	public Map<String, Function> getFunctions() {
		HashMap<String, Function> functions = new HashMap<String,Function>();
		functions.put("secAuth", new AuthenticationPropertyFunction());
		functions.put("csrfInput", new CsrfTokenInputTagFunction());
		functions.put("hasRole", new HasRoleFunction());
		return functions;
	}
		
		public static class HasRoleFunction implements Function
		{
			@Override
			public List<String> getArgumentNames() {
				return Arrays.asList("roleName");
			}

			@Override
			public Object execute(Map<String, Object> args) {
				Collection<? extends GrantedAuthority> authorities = getAuthenticationObject().getAuthorities();

				Object roleNameO = args.get("roleName");
				Preconditions.checkNotNull(roleNameO);
				String roleName = "ROLE_" + roleNameO.toString();
				
				return authorities.stream().anyMatch(ga -> ga.getAuthority().equals(roleName));
			}
			
		}

	public static class AuthenticationPropertyFunction implements Function
	{

		@Override
		public List<String> getArgumentNames() {
			return Arrays.asList("propertyPath");
		}
		@Override
		public Object execute(Map<String, Object> args)
		{
			String propertyPath = args.get("propertyPath").toString();
			return getAuthenticationProperty(getAuthenticationObject(), propertyPath);
		}

		public static Object getAuthenticationProperty(final Authentication authentication, final String property) {

	        if (logger.isTraceEnabled()) {
	            logger.trace("Reading property \"{}\" from authentication object.", property);
	        }

	        if (authentication == null) {
	            return null;
	        }

	        try {

	            final BeanWrapperImpl wrapper = new BeanWrapperImpl(authentication);
	            final Object propertyObj = wrapper.getPropertyValue(property);

	            if (logger.isTraceEnabled()) {
	                logger.trace("Property \"{}\" obtained from authentication object " +
	                		"for user \"{}\". Returned value of class {}.", property, authentication.getName(),
	                                (propertyObj == null? null : propertyObj.getClass().getName()));
	            }

	            return propertyObj;

	        } catch (BeansException e) {
	            logger.error("Error retrieving value for property {} of authentication object of class {}", property, authentication.getClass().getName(), e);
	            return null;
	        }
	    }
		private static final Logger logger = LoggerFactory.getLogger(SpringSecPebbleExtension.AuthenticationPropertyFunction.class);

	}

	public static class CsrfTokenInputTagFunction implements Function
	{

		@Override
		public List<String> getArgumentNames() {
			return null;
		}

		@Override
		public Object execute(Map<String, Object> arg0)
		{
			CsrfToken token = (CsrfToken) ViewUtils.getRequest().getAttribute("_csrf");
			String output = String.format("<input type='hidden' id='csrf' name='%s' value='%s' >", token.getParameterName(), token.getToken());
			return new SafeString(output);
		}

	}

	public static Authentication getAuthenticationObject()
	{

        if (logger.isTraceEnabled()) {
            logger.trace("Obtaining authentication object.");
        }

        if ((SecurityContextHolder.getContext() == null)) {
            if (logger.isTraceEnabled()) {
                logger.trace("No security context found, no authentication object returned.");
            }
            return null;
        }
        final Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication.getPrincipal() == null) {
            if (logger.isTraceEnabled()) {
                logger.trace("No authentication object found in context.");
            }
            return null;
        }

        if (logger.isTraceEnabled()) {
            logger.trace("Authentication object of class {} found in context for user \"{}\".",
            				authentication.getClass().getName(), authentication.getName() );
        }

        return authentication;
    }
    private static final Logger logger = LoggerFactory.getLogger(SpringSecPebbleExtension.class);
}