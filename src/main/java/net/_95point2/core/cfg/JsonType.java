package net._95point2.core.cfg;

import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.internal.util.ReflectHelper;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;
import org.postgresql.util.PGobject;

import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.google.common.base.Strings;
import com.rits.cloning.Cloner;

import lombok.extern.slf4j.Slf4j;
import net._95point2.core.X;

@Slf4j
public class JsonType implements UserType, ParameterizedType
{
	private static final int[] TYPES = new int[] { Types.VARCHAR };
	public static final String CLASS_TYPE = "classType";
	public static final String SQL_TYPE = "sqlType";
	private final ObjectMapper objectMapper = new ObjectMapper();
	private final MapLikeType mapType = objectMapper.getTypeFactory().constructMapLikeType(Map.class, String.class, Object.class);
	private final CollectionLikeType listType = objectMapper.getTypeFactory().constructCollectionLikeType(ArrayList.class, Object.class);
	private static final Cloner cloner = new Cloner();

	private Class<?> classType;

	@Override
	public void setParameterValues(Properties params) {
		String classTypeName = params.getProperty(CLASS_TYPE);
		try {
			this.classType = ReflectHelper.classForName(classTypeName, this.getClass());
		} catch (ClassNotFoundException cnfe) {
			throw new HibernateException("classType not found", cnfe);
		}
	}

	@Override
	public int[] sqlTypes() {
		return TYPES;
	}

	@Override
	public Class returnedClass() {
		return classType;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException
	{
		if( x == y ){
			return true;
		}

		if( (!classType.isInstance(x)) || (!classType.isInstance(y))  ){
			return false;
		}

		return x.equals(y);
	}

	@Override
	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {

		String json = rs.getString(names[0]);
		if(Strings.isNullOrEmpty(json)){
			return null;
		}
		
		try
		{
			return tentativeUnmarshall(json);
		}
		catch(IOException ioe)
		{
			throw new HibernateException("During JSON deserialize: " + ioe.getMessage(), ioe);
		}
		
	}

	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {

		if(value == null ){
			st.setNull(index, Types.NULL);
			return;
		}
		
		if(value instanceof Collection && ((Collection) value).size() == 0){
			st.setNull(index, Types.NULL);
			return;
		}

		try
		{
			String json = objectMapper.writeValueAsString(value);
			PGobject jsonObject = new PGobject();
			jsonObject.setType("jsonb");
			jsonObject.setValue(json);
			st.setObject(index, jsonObject);
		}
		catch (JsonProcessingException e)
		{
			log.error("Error creating JSON for {}, {}", classType, value, e);
			throw X.error("Database Error: JT-01");
		}
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException
	{
		if(value == null) {
			return null;
		}
		log.debug("Deep cloning {}", value.getClass().getSimpleName());
		return cloner.deepClone(value);
	}

	@Override
	public boolean isMutable() {
		return true;
	}

	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		try {
			Serializable out = objectMapper.writeValueAsString(value);
			log.info("{}", out);
			return out;
			
		} catch (JsonProcessingException e) {
			log.error("Error creating JSON for {}, {}", classType, value, e);
			throw X.error("Database Error: JT-02", e);
		}
	}

	@Override
	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		try
		{
			return tentativeUnmarshall((String)cached);
		}
		catch (Exception e)
		{
			log.error("Error retrieving {} from JSON {}", classType, cached, e);
			throw X.error("Database Error: JsonType-assemble", e);
		}
	}

	@Override
	public Object replace(Object original, Object target, Object owner)
			throws HibernateException {
		return deepCopy(original);
	}
	
	
	public Object tentativeUnmarshall(String json) throws IOException
	{
		// first try Map type if the classType suggests it
		if(Map.class.isAssignableFrom(classType))
		{
			try
			{
				return objectMapper.readValue(json, mapType);
			}
			catch (JsonMappingException | JsonParseException e) {
				log.warn("Error retrieving from JSON as Map ({})", json, e);
			}
		}
		
		try
		{
			return objectMapper.readValue(json, classType);
		}
		catch (JsonMappingException | JsonParseException e) {
			log.error("Error retrieving from JSON by type {} ({})", classType, json);
		}
		
		// if still not successful, try List?
		try
		{
			return objectMapper.readValue(json, listType);
		}
		catch(JsonMappingException | JsonParseException e)
		{
			log.warn("Error retrieving from JSON as List ({})", json, e);
		}
		
		throw new HibernateException("Null result for JSON deserialize of JSON: " + json);
	}
	
}