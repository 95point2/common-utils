package net._95point2.core.merge;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Builder;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class SimpleThing
{
    private Long id; 
    private String name;
    private Boolean enabled;
    private SimpleThing.Orientation orientation;
    
    public static enum Orientation {
        Horizontal, Vertical, Other
    }
    
    public String toString(){
        return (id == null ? "<new>" : id) + "[" + name + "]";
    }
}
