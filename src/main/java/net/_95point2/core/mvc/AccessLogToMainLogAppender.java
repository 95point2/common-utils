package net._95point2.core.mvc;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import ch.qos.logback.access.spi.IAccessEvent;
import ch.qos.logback.core.OutputStreamAppender;
import ch.qos.logback.core.encoder.Encoder;
import ch.qos.logback.core.encoder.LayoutWrappingEncoder;
import ch.qos.logback.core.status.Status;
import ch.qos.logback.core.status.StatusListener;

public abstract class AccessLogToMainLogAppender extends OutputStreamAppender<IAccessEvent> implements StatusListener
{
	private Charset charset = Charset.defaultCharset();
	
	public abstract void log(String msg);
	public abstract void status(Status status);
	
	@Override
	public void start() {
		setOutputStream(new FakeStringBufferOutputStream());
		super.start();
	}
	
	@Override
	public void addStatusEvent(Status status) {
		status(status);
	}
	
	@Override
	public void doAppend(IAccessEvent eventObject) {
		/* synchronized, and relying on the immediateFlush=true property of the encoder to do:
		 * 1. write(byte[])
		 * 2. followed by a flush() 
		 * which gives us a single logging statement from our fake OutputStream
		 */
		synchronized (this) {
			super.doAppend(eventObject);
		}
	}
	
	@Override
	public void setEncoder(Encoder<IAccessEvent> encoder) {
		if(encoder instanceof LayoutWrappingEncoder){
			LayoutWrappingEncoder<IAccessEvent> lwe = (LayoutWrappingEncoder<IAccessEvent>) encoder;
			Charset charset = lwe.getCharset();
			if(charset != null){
				this.charset = charset;
			}
			lwe.setImmediateFlush(true);
		}
		else {
			throw new IllegalArgumentException("Only supports Encoders that extend LayoutWrappingEncoder as we need the immediateflush behaviour");
		}
		super.setEncoder(encoder);
	}
	
	public class FakeStringBufferOutputStream extends OutputStream
	{
		StringBuilder sb = new StringBuilder();
		
		@Override
		public void flush() throws IOException {
			AccessLogToMainLogAppender.this.log(sb.toString());
			sb.delete(0, sb.length());
		}
		@Override
		public void write(byte[] bytes) throws IOException {
			sb.append(new String(bytes, AccessLogToMainLogAppender.this.charset));
		}
		
		@Override
		public void write(int b) throws IOException {
			throw new UnsupportedOperationException("Only supports write(byte[] bytes)");
		}
		
		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			throw new UnsupportedOperationException("Only supports write(byte[] bytes)");
		}
	}
}
