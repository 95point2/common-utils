package net._95point2.core.cfg;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import net._95point2.core.X.HttpException;

import org.slf4j.MDC;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.NestedServletException;

@ControllerAdvice(annotations=RestController.class)
@Slf4j
public class ExceptionHandlers 
{
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex)
    {
        log.warn(ex.getMessage(), ex);
        
        Throwable mostSpecificCause = ex.getMostSpecificCause();
        ErrorMessage errorMessage;
        if (mostSpecificCause != null) {
            String exceptionName = mostSpecificCause.getClass().getName();
            String message = mostSpecificCause.getMessage();
            errorMessage = new ErrorMessage(exceptionName, message);
        } else {
            errorMessage = new ErrorMessage(ex.getMessage());
        }
        return new ResponseEntity(errorMessage, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(HttpMessageNotWritableException.class)
    public ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageNotWritableException ex)
    {
        log.error(ex.getMessage(), ex);
        
        Throwable mostSpecificCause = ex.getMostSpecificCause();
        ErrorMessage errorMessage;
        if (mostSpecificCause != null) {
            String exceptionName = mostSpecificCause.getClass().getName();
            String message = mostSpecificCause.getMessage();
            errorMessage = new ErrorMessage(exceptionName, message);
        } else {
            errorMessage = new ErrorMessage(ex.getMessage());
        }
        errorMessage.type = "OKButErrorWriting";
        return new ResponseEntity(errorMessage, HttpStatus.OK);
    }
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex)
    {
        log.warn(ex.getMessage());
        
        Map<String,Object> map = new HashMap<String, Object>();
        
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        List<ObjectError> globalErrors = ex.getBindingResult().getGlobalErrors();
        for (FieldError fieldError : fieldErrors) {
            map.put(fieldError.getField(), fieldError.getDefaultMessage());
        }
        for (ObjectError objectError : globalErrors) {
            map.put(objectError.getObjectName(), objectError.getDefaultMessage());
        }
        return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
    }
    
    /*
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValidException(NotFoundException ex)
    {
        log.warn(ex.getMessage());
        return new ResponseEntity(ex.getMessage(), HttpStatus.NOT_FOUND);
    }
    */
    
    @ExceptionHandler(HttpException.class)
    public ResponseEntity<Object> handleMHttpException(HttpException httpException)
    {
        log.warn("{} [{}] {}", httpException.getStatus().getReasonPhrase(), String.valueOf(httpException.getStatus().value()), httpException.getMessage(), httpException );
        
        try
        {
        	Throwable mostSpecificCause = httpException.getMostSpecificCause();
            ErrorMessage errorMessage;
            if (mostSpecificCause != null) {
                String exceptionName = mostSpecificCause.getClass().getName();
                String message = mostSpecificCause.getMessage();
                errorMessage = new ErrorMessage(exceptionName, message);
            } else {
                errorMessage = new ErrorMessage(httpException.getMessage());
            }
            return new ResponseEntity(errorMessage, httpException.getStatus());
        }
        catch(Exception e)
        {
        	log.warn("Error processing exception: ", e);
        	return new ResponseEntity(httpException.getMessage(), httpException.getStatus());
        }
    }
    
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleRuntimeException(RuntimeException e)
    {
        log.error("RuntimeException - {}", e.getMessage(), e );
        
        return new ResponseEntity(new ErrorMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
   
    @ExceptionHandler({org.springframework.web.util.NestedServletException.class})
    public ResponseEntity handleSpringDataExceptionNSE(NestedServletException nse)
    {
    	log.error("Exception Handled: {}/{}/../{} - {}", nse.getClass().getSimpleName(), nse.getCause().getClass().getSimpleName(), nse.getRootCause(), nse.getMessage(), nse );
		Throwable root = nse.getRootCause();
		log.error("Root Cause: {}", root.getMessage(), root);
    	return new ResponseEntity(root.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler({org.springframework.expression.spel.SpelEvaluationException.class})
    public ResponseEntity handleSPELException(SpelEvaluationException e)
    {
    	log.error("Exception Handled: {}/{} - {}", e.getClass().getSimpleName(), e.getCause().getClass().getSimpleName(), e.getMessage() );
		//Throwable root = nse.getRootCause();
    	return new ResponseEntity("An error occured. technical reason \"" + e.getMessage() + " \"request ID is: " + MDC.get("key"), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    
}
