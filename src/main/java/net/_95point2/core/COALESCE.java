package net._95point2.core;

import java.util.Collections;

public class COALESCE 
{
	public static <E> E list(E object)
	{
		return (E) o(Collections.EMPTY_LIST, object);
	}
	
	public static <E> E map(E object)
	{
		return (E) o(Collections.EMPTY_MAP, object);
	}
	
	public static <E> E set(E object)
	{
		return (E) o(Collections.EMPTY_SET, object);
	}
	
	public static <E> E o(E o, E def)
	{
		return o == null ? def : o;
	}
}
