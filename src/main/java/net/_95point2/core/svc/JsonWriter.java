package net._95point2.core.svc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class JsonWriter
{
    private ObjectMapper om;
    
    @Autowired
    public JsonWriter(ObjectMapper objectMapper){
      om  = objectMapper;
    }
    
    public String write(Object o) throws JsonProcessingException{
        return om.writeValueAsString(o);
    }
}
