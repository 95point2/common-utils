package net._95point2.core.mvc;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.StdConverter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JSPageModelProvider implements HandlerMethodArgumentResolver 
{
	public static final String MODEL_ATTRIBUTE_NAME = "jsPageModel";
	
	@Override
	public Object resolveArgument(MethodParameter param, ModelAndViewContainer mvc, NativeWebRequest req,
			WebDataBinderFactory binder) throws Exception 
	{
		if(!isJSPageModelParam(param)){
			return null;
		}
		JSPageModel jspm = (JSPageModel) mvc.getModel().get(MODEL_ATTRIBUTE_NAME);
		if(jspm == null){
			jspm = new JSPageModel();
			mvc.getModel().addAttribute(MODEL_ATTRIBUTE_NAME, jspm);
		}
		return jspm;
	}

	@Override
	public boolean supportsParameter(MethodParameter param) {
		return isJSPageModelParam(param);
	}
	
	private boolean isJSPageModelParam(MethodParameter param) {
		return param.getParameterType() == JSPageModel.class;
	}
	
	/**
	 * The JSPageModel to be serialised in-line. 
	 * Doesn't implement Map as it doesn't get past the default model map handlermethodargumentresolver MapMethodProcessor
	 * So it uses a convertor to allow it to be emitted as a map => js object
	 * @author rob
	 *
	 */
	@JsonSerialize(converter=JSPageModelSerializingConvertor.class)
	public static class JSPageModel
	{
		private final Map<String,Object> backingMap = new HashMap<>();
	
		
		public boolean containsKey(Object key) {
			return backingMap.containsKey(key);
		}
	
		
		public Object get(Object key) {
			return backingMap.get(key);
		}
	
		public Object put(String key, Object value) {
			return backingMap.put(key, value);
		}
	
		public Object remove(Object key) {
			return backingMap.remove(key);
		}
	
		public void putAll(Map<? extends String, ? extends Object> m) {
			backingMap.putAll(m);
		}
	}
	
	/**
	 * We can't expose JSPageModel as a Map, because a handlermethodresolver earlier in the chain picks it up a the base model map.
	 * So we just expose it as a map during json serialisation.
	 * @author rob
	 *
	 */
	public static class JSPageModelSerializingConvertor extends StdConverter<JSPageModel, Map<String,Object>>
	{
		@Override
		public Map<String, Object> convert(JSPageModel value) {
			return value.backingMap;
		}
	}
}