package net._95point2.core;

import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpStatus;

public class X 
{
	public static HttpException badReq(String reason)
	{
		return new HttpException(HttpStatus.BAD_REQUEST, reason);
	}
	
	public static HttpException conflict(String reason)
	{
		return new HttpException(HttpStatus.CONFLICT, reason);
	}
	
	public static HttpException unauthorised(String reason) {
		return new HttpException(HttpStatus.UNAUTHORIZED, reason);
	}
	public static HttpException unauthorised(String reason, Throwable t) {
		return new HttpException(HttpStatus.UNAUTHORIZED, reason, t);
	}
	
	public static HttpException error(String reason)
	{
		return new HttpException(HttpStatus.INTERNAL_SERVER_ERROR, reason);
	}
	
	public static HttpException error(String reason, Throwable t)
	{
		return new HttpException(HttpStatus.INTERNAL_SERVER_ERROR, reason, t);
	}
	
	public static HttpException paymentRequired(String reason)
	{
		return new HttpException(HttpStatus.PAYMENT_REQUIRED, reason);
	}
	
	public static HttpException notFound(String reason)
	{
		return new HttpException(HttpStatus.NOT_FOUND, reason);
	}
	
	public static class HttpException extends NestedRuntimeException
	{
		private HttpStatus status;
		
		public HttpException(HttpStatus status, String message)
		{
			super(message);
			this.status = status;
		}
		
		public HttpException(HttpStatus status, String message, Throwable cause)
		{
			super(message, cause);
			this.status = status;
		}
		
		public HttpStatus getStatus() {
			return status;
		}
	}
}
