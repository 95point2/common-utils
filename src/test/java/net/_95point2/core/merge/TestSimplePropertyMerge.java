package net._95point2.core.merge;

import java.util.Map;

import org.junit.Test;

import com.google.common.collect.Multimap;

import net._95point2.core.U; 
import net._95point2.core.merge.Merge.MergeException;
import net._95point2.core.merge.Merge.MergeResult;
import net._95point2.core.merge.Merge.Merger;
import net._95point2.core.merge.SimpleThing.Orientation;

import static org.assertj.core.api.Assertions.assertThat;

public class TestSimplePropertyMerge
{
    @Test
    public void test() throws MergeException
    {
        // 1. define merge configuration, permitting some properties 
        Merger MERGE_USER_PROPERTIES = new Merge.Merger().with(SimpleThing.class, "name", "orientation"); // can't change id or enabled with this.
        
        // 2. base object
        SimpleThing t1_disabled = SimpleThing.builder().id(1L).name("T1").enabled(false).orientation(Orientation.Horizontal).build();
        
        // 3. changes incoming from user
        Map<String, Object> t1_changes = U.omap("id", "999L" ) // should be ignored
                        .and("name", "T1-b") // should be changed
                        .and("orientation", Orientation.Vertical) // should be changed
                        .and("enabled", true) // should be ignored
                    .done();
        
        // 4. merge incoming to existing
        Multimap<String, MergeResult> merge = Merge.merge(t1_changes, t1_disabled, MERGE_USER_PROPERTIES);
        
        // 5. get default change String
        String changeLogString = Merge.getChangeLogString(merge);
        
        // 6. check 
        assertThat(t1_disabled.getEnabled()).isFalse();
        assertThat(t1_disabled.getId()).isEqualTo(1L);
        assertThat(t1_disabled.getOrientation()).isEqualTo(Orientation.Vertical);
        assertThat(t1_disabled.getName()).isEqualTo("T1-b");
        
        // 7. check default changelog string.
        assertThat(changeLogString).isEqualTo("orientation changed to \"Vertical\", name changed to \"T1-b\"");
        
    }

}
