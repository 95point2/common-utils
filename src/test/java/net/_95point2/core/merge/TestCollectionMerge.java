package net._95point2.core.merge;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Test;

import com.google.common.base.Joiner;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import net._95point2.core.merge.Merge.MergeException;
import net._95point2.core.merge.Merge.MergeResult;
import net._95point2.core.merge.Merge.Merger;
import net._95point2.core.merge.SimpleThing.Orientation;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Collectors.toMap;

@Slf4j
public class TestCollectionMerge 
{
    @Data @Builder @NoArgsConstructor @AllArgsConstructor
    public static class ThingWithSimpleCollection
    {
        private Long id;
        private Set<String> colours; 
    }
    
    @Test
    public void testSimpleCollection() throws MergeException
    {
        // 1. define merge configuration, permitting some properties 
        Merger MERGE_USER_PROPERTIES = new Merge.Merger()
                                            .with(ThingWithSimpleCollection.class, "colours");
        
        // 2. base object
        ThingWithSimpleCollection twsc = ThingWithSimpleCollection.builder().id(5L)
                                                .colours(Sets.newHashSet("red", "yellow"))
                                                .build();
        
        
        // 3. changes incoming from user
        ThingWithSimpleCollection incoming = ThingWithSimpleCollection.builder().id(5L)
                .colours(Sets.newHashSet("red", "yellow", "cerulean"))
                .build();
        
        // 4. merge incoming to existing
        Multimap<String, MergeResult> merge = Merge.merge(incoming, twsc, MERGE_USER_PROPERTIES);
        
        // 5. get default change String
        String changeLogString = Merge.getChangeLogString(merge);
        
        // 6. check 
        assertThat(twsc.getId()).isEqualTo(5L);
        for(String colour : new String[]{"red", "yellow", "cerulean"})
        {
            assertThat(twsc.getColours()).contains(colour);
        }
        
        assertThat(twsc.getColours().size()).isEqualTo(3);
        
        assertThat(changeLogString).isEqualTo("colours changed to \"[red,yellow,cerulean]\"");
        
           
        // 7b. and no others
        assertThat(merge.size()).isEqualTo(1);
        
    }
    
    @Data @Builder @NoArgsConstructor @AllArgsConstructor
    public static class ThingWithNonSimpleCollection
    {
        private Long id;
        private String name;
        private Set<SimpleThing> things;
    }
    
    @Test
    public void testCollectionOfMergableThings() throws MergeException 
    {
        
        // 1. define merge configuration, permitting some properties 
        Merger MERGE_TWNSC_USER_PROPERTIES = new Merge.Merger()
                                            .with(ThingWithNonSimpleCollection.class, "name", "things")
                                            .withMergableCollections(ThingWithNonSimpleCollection.class, "things", SimpleThing.class)
                                            
                                            .with(SimpleThing.class, 
                                                    SimpleThing::getId , 
                                                    SimpleThing::toString, 
                                                    "name", "orientation");
                                            ; // only name
                                            
         
        
        // 2. base object
        ThingWithNonSimpleCollection existing = ThingWithNonSimpleCollection.builder().id(2L).name("TWNSC-1")
                                    .things(Sets.newHashSet(
                                            SimpleThing.builder().id(101L).name("Raphael").enabled(false).orientation(Orientation.Horizontal).build(),
                                            SimpleThing.builder().id(102L).name("Michaelangelo").enabled(false).orientation(Orientation.Horizontal).build(),
                                            SimpleThing.builder().id(103L).name("Leonardo").enabled(false).orientation(Orientation.Horizontal).build()
                                     ))
                                    .build();
        
        
        // 3. changes incoming from user
        ThingWithNonSimpleCollection incoming = ThingWithNonSimpleCollection.builder().id(2L).name("My TWNSC")
                .things(Sets.newHashSet(
                        SimpleThing.builder().id(101L).name("Raphael").enabled(false).orientation(Orientation.Horizontal).build(),
                        SimpleThing.builder().id(102L).name("Jim").enabled(false).orientation(Orientation.Horizontal).build(),
                        SimpleThing.builder().id(null).name("Donnatello").enabled(false).orientation(Orientation.Horizontal).build()
                 ))
                .build();

        
        // 4. merge incoming to existing
        Multimap<String, MergeResult> merge = Merge.merge(incoming, existing, MERGE_TWNSC_USER_PROPERTIES);
        log.warn("{}", Merge.getChangeLogString(merge));
        assertThat(merge.get("name").stream().map(mr -> mr.getLogEntry()).collect(Collectors.joining())).isEqualTo("name changed to \"My TWNSC\"");
        assertThat(merge.size()).isEqualTo(4);
        
        
        
    }

}
