package net._95point2.core.cfg;

public class StringArrType extends ArrayType 
{
	@Override
	public String getSqlType() {
		return "varchar";
	}
}
