package net._95point2.core.merge;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import lombok.Data;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import net._95point2.core.merge.Merge.MergeClassProperties;

@Slf4j
public class Merge {
	public static <M> Multimap<String, MergeResult> merge(Object source, M dest, Merger merger) throws MergeException {
		// Preconditions.checkArgument(source instanceof Map || dest.getClass() == source.getClass()); // we have use case for "similar" types being merged? protect/prevent?
		Multimap<String, MergeResult> result = HashMultimap.create();
		merge(source, dest, merger, result, null);
		return result;
	}

	private static <M> void merge(Object source, M dest, Merger merger, Multimap<String, MergeResult> changeLog,
			String propertyPrefix) throws MergeException {
		//Preconditions.checkArgument(source instanceof Map || dest.getClass() == source.getClass());

		BeanInfo beanInfo;
		try {
			if(source instanceof Map && dest instanceof Map) {
				throw new MergeException("Cannot merge two maps: " + dest.getClass().getSimpleName(), new Throwable());
			}
			if (dest instanceof Map) {
				beanInfo = Introspector.getBeanInfo(source.getClass());
			} else {
				beanInfo = Introspector.getBeanInfo(dest.getClass());
			}
		}catch (IntrospectionException e) {
			throw new MergeException("Problem inspecting bean: " + dest.getClass().getSimpleName(), e);
		}

		MergeClassProperties mcp = merger.mergeConf.get(dest.getClass());

		if (mcp == null) {
			log.warn("No merge properties for target class {} - not merging", dest.getClass().getSimpleName());
			return;
		}

		for (PropertyDescriptor descriptor : beanInfo.getPropertyDescriptors()) {

			if (mcp.mergeProperties.contains(descriptor.getName())) {
				// just a plain thing
				if (!hasPropertyOrKey(descriptor, source)) {
					log.debug("1. Property or Key not found: {}#{}", source, descriptor.getName());
					continue;
				}

				Object srcVal;
				Object destVal;
				try {
					srcVal = get(descriptor.getName(), source, merger);
					destVal = get(descriptor, dest);
				}
				catch(IllegalArgumentException iae) {
					log.warn("Cannot access some properties", iae);
					continue;
				}

				// will attempt to merge this
				if (BeanUtils.isSimpleValueType(descriptor.getPropertyType())
						|| mcp.mergeRefsProperties.contains(descriptor.getName())
						|| descriptor.getPropertyType().isEnum()) {
					if (simpleEquals(srcVal, destVal)) {
						// these properties are equal.
					} else {
						try {
							descriptor.getWriteMethod().invoke(dest, srcVal);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
							throw new MergeException("Problem writing new value: " + descriptor.getName(), e);
						}

						String prop = Strings.isNullOrEmpty(propertyPrefix) ? descriptor.getName()
								: propertyPrefix + "." + descriptor.getName();
						changeLog.put(prop,
								MergeResult.builder().property(prop)
										.oldVal(destVal == null ? null : Objects.toString(destVal))
										.newVal(srcVal == null ? null : Objects.toString(srcVal)).build());
					}
				} else if (srcVal == null && destVal != null) // doesn't matter
																// what type it
																// is - deleted!
				{
					try {
						descriptor.getWriteMethod().invoke(dest, srcVal);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						throw new MergeException("Problem writing new value: " + descriptor.getName(), e);
					}

					String prop = Strings.isNullOrEmpty(propertyPrefix) ? descriptor.getName()
							: propertyPrefix + "." + descriptor.getName();
					changeLog.put(prop,
							MergeResult.builder().property(prop).oldVal(destVal.toString()).newVal(null).build());
				} else if (Collection.class.isAssignableFrom(descriptor.getPropertyType())) {
					log.debug("is a collection: {}/{}", descriptor.getPropertyType(), descriptor.getName());

					Set srcSet = new HashSet();
					if (srcVal != null && srcVal instanceof Collection) {
						srcSet.addAll((Collection) srcVal);
					}

					Set dstSet = new HashSet();
					if (destVal != null && destVal instanceof Collection) {
						dstSet.addAll((Collection) destVal);
					}

					if (mcp.mergableCollectionProperties.containsKey(descriptor.getName())) 
					{
						String prop = Strings.isNullOrEmpty(propertyPrefix) ? descriptor.getName()
								: propertyPrefix + "." + descriptor.getName();

						Class<?> targetCollectionClass = (Class<?>) mcp.mergableCollectionProperties.get(descriptor.getName());
						MergeClassProperties collElemMCP = merger.mergeConf.get(targetCollectionClass);
						// it's a mergeable collection.
						log.debug("mergable collection: {}/{}/{}", descriptor.getPropertyType(), descriptor.getName(), targetCollectionClass);
						ArrayList<MergeResult> cl = mergeCollection((Collection) destVal, (Collection) srcVal, collElemMCP, merger, descriptor.getName());
						cl.forEach(mr -> changeLog.put(prop, mr));
																							// finish?
					} else if (!srcSet.equals(dstSet)) {
						log.debug("non-mergable collection: {}/{}", descriptor.getPropertyType(), descriptor.getName());
						Collection newColl = newColl(descriptor.getPropertyType());

						newColl.addAll(srcSet);
						try {
							descriptor.getWriteMethod().invoke(dest, newColl);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
							throw new MergeException("Problem writing new value: " + descriptor.getName(), e);
						}

						String prop = Strings.isNullOrEmpty(propertyPrefix) ? descriptor.getName()
								: propertyPrefix + "." + descriptor.getName();
						changeLog.put(prop, MergeResult.builder().property(prop).oldVal(arrayStr(destVal))
								.newVal(arrayStr(srcSet)).build());
					} else {
						// noop, these collections are SET-equal (maybe not list
						// equal)
					}

				} else {
					if (merger.mergeConf.containsKey(descriptor.getPropertyType())) {
						Object srcField;
						Object destField;
						
						try {
							srcField = get(descriptor.getName(), source, merger);
							destField = get(descriptor, dest);
						}
						catch(IllegalArgumentException iae) {
							log.warn("Cannot access some properties", iae);
							continue;
						}
						

						if (srcField != null && destField != null) {
							merge(srcField, destField, merger, changeLog, descriptor.getName());
						}
					}
				}
			}
		}
	}

	private static Collection newColl(Class<?> propertyType) {
		if (Set.class.isAssignableFrom(propertyType)) {
			return new HashSet();
		}
		if (List.class.isAssignableFrom(propertyType)) {
			return new ArrayList();
		}

		throw new UnsupportedOperationException("Cannot create new collection: " + propertyType.getSimpleName());
	}

	private static <T, K> ArrayList<MergeResult> mergeCollection(Collection<T> existing, Collection<T> incoming,
			MergeClassProperties<T, K> mcp, Merger merger, String collectionProperty) throws MergeException {
		// sub-things
		ArrayList<MergeResult> changes = new ArrayList<>();
		final Map<K, T> existingLkp = existing.stream().collect(toMap(mcp.idMapper, Function.identity()));
		final Map<K, T> incomingLkp = incoming.stream().filter(t -> mcp.idMapper.apply(t) != null)
				.collect(toMap(mcp.idMapper, Function.identity()));

		final Set<T> added = incoming.stream().filter(t -> mcp.idMapper.apply(t) == null).collect(toSet());
		if (added.size() > 0) {
			// enact addition
			added.stream().forEach(st -> {
				changes.add( MergeResult.builder().property(collectionProperty + "[+]").newVal(mcp.descMapper.apply(st)).build());
				existing.add(st);
			});
		}

		final Set<K> removedIds = Sets.difference(existingLkp.keySet(), incomingLkp.keySet());
		if (removedIds.size() > 0) {
			// enact removal
			for (Iterator<T> it = existing.iterator(); it.hasNext();) {
				T st = it.next();
				if (removedIds.contains(mcp.idMapper.apply(st))) {
					changes.add(MergeResult.builder().property(collectionProperty + "[-]").oldVal(mcp.descMapper.apply(st)).build());
					it.remove();
				}
			}
		}

		final Set<K> possibleChangedIds = Sets.intersection(existingLkp.keySet(), incomingLkp.keySet());

		for (K id : possibleChangedIds) {
			T existingST = existingLkp.get(id);
			String marker = mcp.descMapper.apply(existingST);
			T incomingST = incomingLkp.get(id);
			// merge carries out the merging of changes
			Multimap<String, MergeResult> mergeST = Merge.merge(incomingST, existingST, merger);
			if (mergeST.size() > 0) {
				for(Entry<String, MergeResult> entry : mergeST.entries())
				{
					String prop = collectionProperty + "[" + marker + "]." + entry.getKey();
					MergeResult subMergeResult = entry.getValue();
					changes.add(MergeResult.builder().property(prop).oldVal(subMergeResult.getOldVal()).newVal(subMergeResult.getNewVal()).build());
				}
			}
		}

		return changes;
	}

	private static String arrayStr(Object srcVal) {
		if (srcVal == null) {
			return null;
		}
		if (srcVal instanceof Iterable) {
			return "[" + Joiner.on(',').join((Iterable<?>) srcVal) + "]";
		} else {
			return srcVal.toString();
		}
	}

	private static boolean hasPropertyOrKey(PropertyDescriptor descriptor, Object source) {
		if (source instanceof Map) {
			return ((Map) source).containsKey(descriptor.getName());
		} else {
			return true; // else it must be an object matching dest type
							// (assumption for now)
		}
	}

	private static boolean isNullOrEmpty(Object val) {
		if (val == null) {
			return true;
		}
		if (val instanceof String) {
			return Strings.isNullOrEmpty((String) val);
		}
		if (val instanceof Collection) {
			return ((Collection) val).isEmpty();
		}
		return false;
	}

	public static class Merger {
		Map<Class<?>, MergeClassProperties> mergeConf = new HashMap<>();

		public Merger with(Class<?> clazz, String... props) {
			mergeConf.put(clazz, new Merge.MergeClassProperties().addMergeProperties(props));
			return this;
		}

		public <T, K> Merger with(Class<T> clazz, Function<T, K> idMapper, Function<T, String> descMapper,
				String... props) {
			MergeClassProperties mergeClassProperties = new Merge.MergeClassProperties();
			mergeClassProperties.addMergeProperties(props);
			mergeClassProperties.setDescMapper(descMapper);
			mergeClassProperties.setIdMapper(idMapper);
			mergeConf.put(clazz, mergeClassProperties);
			return this;
		}

		public Merger withRefs(Class<?> clazz, String... refs) {
			MergeClassProperties mcp = mergeConf.get(clazz);
			if (mcp == null) {
				mcp = new Merge.MergeClassProperties();
			}
			mcp.addMergeRefsProperties(refs);
			return this;
		}

		public Merger withMergableCollections(Class<?> clazz, String collectionProperty, Class<?> targetClass) {
			MergeClassProperties mcp = mergeConf.get(clazz);
			if (mcp == null) {
				mcp = new Merge.MergeClassProperties();
			}
			mcp.addMergableCollection(collectionProperty, targetClass);
			return this;
		}
		
		/**
		 * Given a class, 
		 * @param clazz
		 * @param exceptThese
		 * @return
		 */
		public Merger withAllExcept(Class<?> clazz, String... exceptThese) {
			Set<String> set = Arrays.stream(exceptThese).collect(Collectors.toSet());
			set.add("class");
			
			Set<String> mergeSet = Arrays.asList(BeanUtils.getPropertyDescriptors(clazz)).stream().map(PropertyDescriptor::getName).filter( prop -> !set.contains(prop) ).collect(Collectors.toSet());
			MergeClassProperties mergeClassProperties = new Merge.MergeClassProperties();
			mergeClassProperties.mergeProperties.addAll(mergeSet);
			mergeConf.put(clazz, mergeClassProperties);
			return this;
		}
	}

	@Data
	public static class MergeClassProperties<T, K> {
		Set<String> mergeProperties = new HashSet<String>();
		Set<String> mergeRefsProperties = new HashSet<String>();
		Map<String,Class<?>> mergableCollectionProperties = new HashMap<String,Class<?>>();
		Function<T, K> idMapper;
		Function<T, String> descMapper;

		public MergeClassProperties<T, K> addMergeProperties(String... props) {
			this.mergeProperties.addAll(Arrays.asList(props));
			return this;
		}

		public MergeClassProperties<T, K> addMergableCollection(String collecrtionProperty, Class<?> targetClass) {
			this.mergableCollectionProperties.put(collecrtionProperty, targetClass);
			return this;
		}

		public MergeClassProperties<T, K> addMergeRefsProperties(String... props) {
			this.mergeRefsProperties.addAll(Arrays.asList(props));
			return this;
		}

	}

	@Data
	@Builder
	public static class MergeResult {
		String property;
		String oldVal;
		String newVal;	

		public String getLogEntry() {
			if (isNullOrEmpty(oldVal) && !isNullOrEmpty(newVal)) {
				return property + " set to \"" + newVal + "\"";
			} else if (isNullOrEmpty(newVal) && !isNullOrEmpty(oldVal)) {
				return property + " deleted " + oldVal;
			} else {
				return property + " changed to \"" + newVal + "\"";
			}
		}
	}

	/**
	 * get using an existing property description
	 * @param prop
	 * @param object
	 * @return
	 */
	public static Object get(PropertyDescriptor prop, Object object) {
		if (object instanceof Map) {
			log.debug("Attempt to access by Map-key: {} from {}", prop.getName(), object);
			return ((Map) object).get(prop.getName());
		}
		try {
			log.debug("Attempt to access by Reflection: {} from {}", prop.getName(), object);

			return prop.getReadMethod().invoke(object);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			log.warn("Cannot read property {} from object {}", prop.getName(), object, e);
			return null;
		}
	}
	
	/**
	 * get based on type
	 * @param name
	 * @param source
	 * @param merger
	 * @return
	 */
	private static Object get(String name, Object source, Merger merger) {
		if (source instanceof Map) {
			log.debug("Attempt to access by Map-key: {} from {}", name, source);
			return ((Map) source).get(name);
		}
		try {
			log.debug("Attempt to access by Reflection: {} from {}", name, source);

			MergeClassProperties mcp = merger.mergeConf.get(source.getClass());
			if(mcp == null) {
				throw new IllegalArgumentException("No mergeclass properties Cannot read from type: " + source.getClass().getSimpleName()); 
			}
			if(!mcp.mergeProperties.contains(name) && !mcp.mergeRefsProperties.contains(name)) {
				throw new IllegalArgumentException(String.format("Mapped property '%s' not found for type: %s", name, source.getClass().getSimpleName()));
			}
			
			BeanInfo beanInfo = Introspector.getBeanInfo(source.getClass());
			Optional<PropertyDescriptor> prop = Arrays.stream(beanInfo.getPropertyDescriptors()).filter( pd -> pd.getName().equals(name) ).findFirst();
			if(prop.isPresent()) {
				return prop.get().getReadMethod().invoke(source);
			}
			else {
				throw new IllegalArgumentException(String.format("Property Descriptor '%s' not found for type: %s", name, source.getClass().getSimpleName()));
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
			log.warn(e.getClass().getSimpleName() + ": " + e.getMessage(), e);
			throw new IllegalArgumentException(String.format("Cannot read '%s' from type: %s", name, source.getClass().getSimpleName()));
		}
		
		
	}

	public static boolean simpleEquals(Object a, Object b) {
		if (a == null && b == null) {
			return true;
		}

		if (a == null || b == null) {
			return false;
		}

		if (a == b) {
			return true;
		}

		if (a.equals(b)) {
			return true;
		}

		return false;
	}

	public static String getChangeLogString(Multimap<String, MergeResult> merge) {
		return merge.values().stream().map(mr -> mr.getLogEntry()).collect(Collectors.joining(", "));
	}

	public static class MergeException extends Exception {
		public MergeException(String message, Throwable cause) {
			super(message, cause);
		}
	}
}
