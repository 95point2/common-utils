package net._95point2.core.merge;

import org.junit.Test;

import com.google.common.collect.Multimap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net._95point2.core.merge.Merge.MergeException;
import net._95point2.core.merge.Merge.MergeResult;
import net._95point2.core.merge.Merge.Merger;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class TestDifferentTypes 
{
	@Test
	public void testDifferentTypes() throws MergeException
	{
		Merger merger = new Merge.Merger().withAllExcept(TypeA.class).withAllExcept(TypeB.class);
		
		TypeA source = TypeA.builder().age(1).name("1").other("1-other").faves(Arrays.asList("@95point2", "@mountresuk")).build();
		TypeB target = new TypeB();
		
		Multimap<String, MergeResult> res = Merge.merge(source, target, merger);
		log.warn(Merge.getChangeLogString(res));
		
		assertThat(target.getAge()).isEqualTo(1);
		assertThat(target.getName()).isEqualTo("1");
		assertThat(target.getOther()).isEqualTo("1-other");
		assertThat(target.getFaves()).containsExactlyInAnyOrder("@95point2", "@mountresuk");
		
		TypeA source2 = TypeA.builder().age(1).name("1").other("1-other").faves(Arrays.asList("@95point2", "@mountresuk", "@twitter")).build();
		TypeB target2 = TypeB.builder().age(1).name("1").other("1-other-B").faves(Arrays.asList("@95point2", "@realDonaldTrump")).build();
		
		Multimap<String, MergeResult> res2 = Merge.merge(source2, target2, merger);
		
		log.warn(Merge.getChangeLogString(res2));
		
		assertThat(target2.getAge()).isEqualTo(1);
		assertThat(target2.getName()).isEqualTo("1");
		assertThat(target2.getOther()).isEqualTo("1-other");
		assertThat(target2.getFaves()).containsExactlyInAnyOrder("@95point2", "@mountresuk", "@twitter");
		
		
		Merger merger3 = new Merge.Merger().withAllExcept(TypeA.class).withAllExcept(TypeC.class);
		
		TypeA source3 = TypeA.builder().age(1).name("1").other("1-other").faves(Arrays.asList("@95point2", "@mountresuk")).build();
		TypeC target3 = new TypeC();
		
		Multimap<String, MergeResult> res3 = Merge.merge(source3, target3, merger3);
		log.warn(Merge.getChangeLogString(res3));
		
		assertThat(target3.getAge()).isEqualTo(1);
		assertThat(target3.getName()).isEqualTo("1");
		
		Merger merger4 = merger3;
		TypeC source4 = TypeC.builder().age(1).name("1").clang(3.14).build();
		TypeA target4 = new TypeA();
		
		Multimap<String, MergeResult> res4 = Merge.merge(source4, target4, merger4);
		log.warn(Merge.getChangeLogString(res4));
		
		assertThat(target4.getAge()).isEqualTo(1);
		assertThat(target4.getName()).isEqualTo("1");
		
		
	}
	
	
	@Data @Builder @AllArgsConstructor @NoArgsConstructor
	public static class TypeA {
		private String name;
		private Integer age;
		private String other;
		private List<String> faves;
	}
	
	@Data @Builder @AllArgsConstructor @NoArgsConstructor
	public static class TypeB {
		private String name;
		private Integer age;
		private String other;
		private List<String> faves;
	}
	
	@Data @Builder @AllArgsConstructor @NoArgsConstructor
	public static class TypeC {
		private String name;
		private Integer age;
		private Double clang;
	}
}
