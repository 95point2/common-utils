package net._95point2.core.merge;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

import org.junit.Test;

import com.google.common.collect.Multimap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Builder;
import net._95point2.core.merge.Merge.MergeException;
import net._95point2.core.merge.Merge.MergeResult;
import net._95point2.core.merge.Merge.Merger;
import net._95point2.core.merge.SimpleThing.Orientation;

public class TestNestedMerge
{
    @Data @Builder @NoArgsConstructor @AllArgsConstructor
    public static class ComplexThing
    {
        private Long id;
        private String name;
        
        private SimpleThing relA;
        private SimpleThing relB;
    }
    
    @Test
    public void testNestedProperties() throws MergeException
    {
        // 1. define merge configuration, permitting some properties 
        Merger MERGE_USER_PROPERTIES = new Merge.Merger()
                                            .with(SimpleThing.class, "name", "orientation") // can't change id or "enabled" with this.
                                            .with(ComplexThing.class, "name", "relA", "relB");
        
        // 2. base object
        ComplexThing existing = ComplexThing.builder().id(2L).name("CT2")
                                    .relA(SimpleThing.builder().id(3L).name("ST-2/3-A").enabled(false).orientation(Orientation.Horizontal).build())
                                    .relB(SimpleThing.builder().id(3L).name("ST-2/4-B").enabled(false).orientation(Orientation.Horizontal).build())
                                    .build();
        
        
        // 3. changes incoming from user
        ComplexThing incoming = ComplexThing.builder().id(2L).name("Vivaldi")
                        .relA(SimpleThing.builder().id(3L).name("Vivaldi-A").enabled(false).orientation(Orientation.Horizontal).build())
                        .relB(SimpleThing.builder().id(3L).name("Vivaldi-B").enabled(false).orientation(Orientation.Vertical).build())
                        .build();
        
        // 4. merge incoming to existing
        Multimap<String, MergeResult> merge = Merge.merge(incoming, existing, MERGE_USER_PROPERTIES);
        
        // 5. get default change String
        String changeLogString = Merge.getChangeLogString(merge);
        
        // 6. check 
        assertThat(existing.getId()).isEqualTo(2L);
        assertThat(existing.getName()).isEqualTo("Vivaldi");
        assertThat(existing.getRelA().getName()).isEqualTo("Vivaldi-A");
        assertThat(existing.getRelB().getName()).isEqualTo("Vivaldi-B");
        
        
        // 7a. check default changelog string contents.
        for(String changeLogPart : new String[]{"relA.name changed to \"Vivaldi-A\"", "relB.orientation changed to \"Vertical\"", "name changed to \"Vivaldi\"", "relB.name changed to \"Vivaldi-B\""})
        {
            assertThat(changeLogString).contains(changeLogPart);
        }
           
        // 7b. and no others
        assertThat(merge.size()).isEqualTo(4);
        
    }
    
    @Test
    public void testDeleteNestedProperty() throws MergeException
    {
        // 1. define merge configuration, permitting some properties 
        Merger MERGE_USER_PROPERTIES = new Merge.Merger()
                                            .with(SimpleThing.class, "name", "orientation") // can't change id or "enabled" with this.
                                            .with(ComplexThing.class, "name", "relA", "relB");
        
        // 2. base object
        ComplexThing existing = ComplexThing.builder().id(2L).name("CT2")
                                    .relA(SimpleThing.builder().id(3L).name("ST-2/3-A").enabled(false).orientation(Orientation.Horizontal).build())
                                    .relB(SimpleThing.builder().id(3L).name("ST-2/4-B").enabled(false).orientation(Orientation.Horizontal).build())
                                    .build();
        
        
        // 3. changes incoming from user
        ComplexThing incoming = ComplexThing.builder().id(2L).name("CT2")
                        .relA(SimpleThing.builder().id(3L).name("ST-2/3-A").enabled(false).orientation(Orientation.Horizontal).build())
                        .relB(null)
                        .build();
        
        // 4. merge incoming to existing
        Multimap<String, MergeResult> merge = Merge.merge(incoming, existing, MERGE_USER_PROPERTIES);
        
        // 5. get default change String
        String changeLogString = Merge.getChangeLogString(merge);
        
        // 6. check 
        assertThat(existing.getId()).isEqualTo(2L);
        assertThat(existing.getName()).isEqualTo("CT2");
        assertThat(existing.getRelA().getName()).isEqualTo("ST-2/3-A");
        assertThat(existing.getRelB()).isNull();
        
        // 7a. check default changelog string contents.
        assertThat(changeLogString).contains("relB deleted");
        
    }

}
