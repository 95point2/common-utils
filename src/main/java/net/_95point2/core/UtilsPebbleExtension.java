package net._95point2.core;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mitchellbosecke.pebble.extension.AbstractExtension;
import com.mitchellbosecke.pebble.extension.Filter;
import com.mitchellbosecke.pebble.extension.Function;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UtilsPebbleExtension extends AbstractExtension
{
	@Override
	public Map<String, Filter> getFilters()
	{
	    HashMap<String, Filter> filters = new HashMap<String,Filter>();
		filters.put("long2Date", new Long2DatePebbleFilter());
		filters.put("toString", new ToStringPebbleFilter());
		filters.put("dataUrlHTML", new DataUrlHTMLPebbleFilter());
		return filters;
	}
	
	public static class Long2DatePebbleFilter implements Filter
	{
		
		@Override
		public Object apply(Object arg0, Map<String, Object> arg1)
		{
		    if(arg0 instanceof Long){
			return new Date((long)arg0);
		    }
		    else if(arg0 instanceof Integer){
			return new Date(((int)arg0)*1000L);
		    }
		    throw new IllegalArgumentException("input is not a long or int");
		}

		@Override
		public List<String> getArgumentNames()
		{
		    return null;
		}
	}
	
	public static class ToStringPebbleFilter implements Filter
	{
		
		public List<String> getArgumentNames() {
			return null;
		}
		
		@Override
		public Object apply(Object arg0, Map<String, Object> arg1)
		{
		    return arg0.toString();
		}
	}
	
	public static class DataUrlHTMLPebbleFilter implements Filter
	{

	    @Override
	    public List<String> getArgumentNames()
	    {
		return null;
	    }

	    @Override
	    public Object apply(Object arg0, Map<String, Object> arg1)
	    {
		return "data:text/html;base64," + Base64.getEncoder().encodeToString(arg0.toString().getBytes());
	    }
	    
	}
}