package net._95point2.core.mvc;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class Toastr
{
	private static final String KEY = "toastr";
        private List<String> info;
	private List<String> success;
	private List<String> warning;
	private List<String> danger;

	public void danger(String msg, Object... args){
		if(danger == null){
			danger=new ArrayList<>();
		}
		danger.add(String.format(msg, args));
	}

	public void info(String msg, Object... args){
		if(info == null){
			info=new ArrayList<>();
		}
		info.add(String.format(msg, args));
	}

	public void success(String msg, Object... args){
		if(success == null){
			success=new ArrayList<>();
		}
		success.add(String.format(msg, args));
	}

	public void warning(String msg, Object... args){
		if(warning == null){
			warning=new ArrayList<>();
		}
		warning.add(String.format(msg, args));
	}

	public static class ToastrProvider  implements HandlerMethodArgumentResolver
	{
		@Override
		public Object resolveArgument(MethodParameter param, ModelAndViewContainer model, NativeWebRequest req, WebDataBinderFactory binder) throws Exception {
			if(!isToastrParam(param)){
				return null;
			}
			
			Toastr toastr = (Toastr) model.getModel().get(KEY);
			if(toastr == null){
				toastr = new Toastr();
				model.getModel().addAttribute(KEY, toastr);
			}
			return toastr;
		}

		@Override
		public boolean supportsParameter(MethodParameter param) {
			return isToastrParam(param);
		}

		private boolean isToastrParam(MethodParameter param) {
			return param.getParameterType() == Toastr.class;
		}
	}

	public List<String> getInfo() {
		return info;
	}
	public List<String> getSuccess() {
		return success;
	}
	public List<String> getWarning() {
		return warning;
	}
	public List<String> getDanger() {
		return danger;
	}

	public static Toastr forInfo(String msg, Object... args){
	    Toastr t = new Toastr();
	    t.info(msg, args);
	    return t;
	}

	public static Toastr forSuccess(String msg, Object... args){
            Toastr t = new Toastr();
            t.success(msg, args);
            return t;
        }

	public static Toastr forWarning(String msg, Object... args){
            Toastr t = new Toastr();
            t.warning(msg, args);
            return t;
        }

	public static Toastr forDanger(String msg, Object... args){
            Toastr t = new Toastr();
            t.danger(msg, args);
            return t;
        }

    public static Toastr flash(RedirectAttributes redirectAttributes)
    {
        Toastr toastr = (Toastr) redirectAttributes.getFlashAttributes().get(Toastr.KEY);
        if(toastr == null){
            toastr = new Toastr();
            redirectAttributes.addFlashAttribute(Toastr.KEY, toastr);
        }

        return toastr;
    }
}