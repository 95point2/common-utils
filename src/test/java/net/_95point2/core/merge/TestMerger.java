package net._95point2.core.merge;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import lombok.Data;
import net._95point2.core.merge.Merge.Merger;

public class TestMerger 
{
	/**
	 * Test building a merge using the withAllExcept(...) method 
	 */
	@Test
	public void testBuildingWithout() 
	{
		Merger merger = new Merge.Merger().withAllExcept(TestSubject.class, "age", "enabled");
		Assertions.assertThat(merger.mergeConf.get(TestSubject.class).mergeProperties).containsExactlyInAnyOrder("id", "name", "description", "other");
		Assertions.assertThat(merger.mergeConf.get(TestSubject.class).mergeProperties).doesNotContain("age", "enabled");
		
		Merger merger2 = new Merge.Merger().withAllExcept(TestSubject.class);
		Assertions.assertThat(merger2.mergeConf.get(TestSubject.class).mergeProperties).containsExactlyInAnyOrder("id", "name", "description", "other", "age", "enabled");
		Assertions.assertThat(merger2.mergeConf.get(TestSubject.class).mergeProperties).doesNotContain("class");
	}
	
	@Data
	public static class TestSubject {
		private int id;
		private String name;
		private String description;
		private boolean enabled;
		private int age;
		private String other;
	}
}
