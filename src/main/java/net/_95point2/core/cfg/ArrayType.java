package net._95point2.core.cfg;

import java.io.Serializable;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.internal.util.ReflectHelper;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import javassist.bytecode.Descriptor.Iterator;

public class ArrayType implements UserType, ParameterizedType
{
	private static final int[] TYPES = new int[] { Types.ARRAY };
	public static final String CLASS_TYPE = "classType";
	public static final String SQL_TYPE = "sqlType";
	
	private Class<?> classType;
	private String sqlType;
	
	public String getSqlType() {
		return sqlType;
	}
	
	@Override
	public void setParameterValues(Properties params) {
		String classTypeName = params.getProperty(CLASS_TYPE);
		try {
			this.classType = ReflectHelper.classForName(classTypeName, this.getClass());
		} catch (ClassNotFoundException cnfe) {
			throw new HibernateException("classType not found", cnfe);
		}
		this.sqlType = params.getProperty(SQL_TYPE);
	}
	
	@Override
	public int[] sqlTypes() {
		return TYPES;
	}

	@Override
	public Class returnedClass() {
		return List.class;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException 
	{
		if( x == y ){
			return true;
		}
		
		if( !(x instanceof AbstractCollection) || !(y instanceof AbstractCollection) ){
			return false;
		}
		
		AbstractCollection listX = (AbstractCollection)x;
		AbstractCollection listY = (AbstractCollection)y;
		
		if(listX.size() != listY.size()){
		    return false;
		}
		
		return listX.equals(listY);
		
	}
	
	@Override
	public int hashCode(Object x) throws HibernateException {
		return Arrays.hashCode( ((List)x).toArray() );
	}

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
		
		/* 
		if(rs.wasNull()){
			return new ArrayList();
		}
		*/
		
		java.sql.Array arr = rs.getArray(names[0]);
		
		if(arr != null){
			Object[] array = (Object[])arr.getArray();
			List<Object> list = new ArrayList( Arrays.asList(array) );
			return list;
		}
		else {
			return new ArrayList();
		}
		
	}

	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
		
		if( value == null ){
			st.setNull(index, TYPES[0]);
			return;
		}
		
		if(! (value instanceof AbstractCollection) ){
			throw new IllegalArgumentException(value.getClass().getSimpleName() + " is not an AbstractCollection");
		}
		
		AbstractCollection coll = (AbstractCollection)value;
		
		if(coll == null || coll.size() == 0 ){
			st.setNull(index, TYPES[0]);
			return;
		}
		
		
		
		Array array = session.connection().createArrayOf(getSqlType(), coll.toArray());
		st.setArray(index, array);
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		if(value == null){
		    return null;
		}
	    if(value instanceof List){
			List l = new ArrayList();
			l.addAll((Collection) value);
			return l;
		}
	    if(value instanceof Set){
			Set l = new HashSet();
			l.addAll((Collection) value);
			return l;
		}
		throw new IllegalArgumentException("Not a List<" + getSqlType() + ">: " + value.getClass());
	}

	@Override
	public boolean isMutable() {
		return true;
	}

	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return new Gson().toJson(value);
	}

	@Override
	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return new Gson().fromJson((String) cached, new TypeToken<ArrayList>() {}.getType());
	}

	@Override
	public Object replace(Object original, Object target, Object owner)
			throws HibernateException {
		return deepCopy(original);
	}
}
